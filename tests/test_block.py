import unittest
from block_chain import Block


class TestBlock(unittest.TestCase):

    def test_compute_hash_returns_hash_like_object(self):
        # GIVEN
        a_block = Block()
        expected_length = 64
        # WHEN
        a_hash = a_block.compute_hash()
        hash_length = len(a_hash)
        # THEN
        self.assertEqual(hash_length, expected_length)

    def test_compute_hash_returns_different_hashes_for_different_data(self):
        # GIVEN
        block_1, block_2 = Block(data="1"), Block(data="2")
        # WHEN
        hash_1, hash_2 = block_1.compute_hash(), block_2.compute_hash()
        # THEN
        self.assertNotEqual(hash_1, hash_2)


if __name__ == '__main__':
    unittest.main()
