import unittest
import application
from block_chain import Block, Blockchain


# ==================================================================================================================== #

class TestRouteAPI(unittest.TestCase):

    def setUp(self) -> None:
        # API mocks
        self.tester = application.app.test_client(self)

        self.return_code_200 = 200
        self.return_code_400 = 400

        self.register_node_route = "/register_node"
        self.add_model_route = "/add_model"
        self.mine_route = "/mine"
        self.get_chain_route = "/get_chain"

        # JSON mocks
        self.an_address = "an_address"
        self.a_model = "a_model"
        self.models = ["model_1", "model_2"]
        self.hash_1 = "00d463628473159378be45f13b6848c5691287430bc65d83c42a3ac397bab77f"
        self.hash_2 = "001163628473159378be45f13b6848c5691287430bc65d83c42a3ac397bab77f"
        self.timestamp_1 = 1582810481.619228
        self.timestamp_2 = 2282810481.619228
        self.nonce_1 = 11
        self.nonce_2 = 12
        self.index_0 = 0
        self.index_1 = 1

        self.empty_json = {}
        self.register_json = {"node_address": self.an_address}
        self.json_for_adding_model = {"node_address": self.an_address, "model_to_add": self.a_model}
        self.json_for_adding_models = {"node_address": self.an_address, "model_to_add": self.models}
        self.block_chain_as_json = [{
            "index": self.index_0,
            "data": "",
            "previous_hash": 0,  # Genesis Block has previous hash 0 by design
            "nonce": self.nonce_1,
            "hash": self.hash_1,
            "timestamp": self.timestamp_1,
        },
            {
                "index": self.index_1,
                "data": self.a_model,
                "previous_hash": self.hash_1,
                "nonce": self.nonce_2,
                "hash": self.hash_2,
                "timestamp": self.timestamp_2,
            }]

        # Blockchain object
        self.block_chain = Blockchain()
        self.block_chain.chain[0].index = self.index_0
        self.block_chain.chain[0].data = ""
        self.block_chain.chain[0].previous_hash = 0
        self.block_chain.chain[0].nonce = self.nonce_1
        self.block_chain.chain[0].hash = self.hash_1
        self.block_chain.chain[0].timestamp = self.timestamp_1

        block_to_add = Block(
            index=self.index_1,
            data=self.a_model,
            previous_hash=self.hash_1,
            nonce=self.nonce_2,
            hash=self.hash_2,
            timestamp=self.timestamp_2,
        )
        self.block_chain.add_block(block_to_add)

    def tearDown(self) -> None:
        # Resets the application variables
        application.PEERS = []
        application.block_chain = application.Blockchain()

    # /register_node route =========================================================================================== #

    def test_register_node_get_200_response_when_registering_with_correct_json(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.register_node_route, json=self.register_json)
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)

    def test_register_node_does_register_new_member_when_he_send_correct_json(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        # THEN
        self.assertIn(self.an_address, application.PEERS)

    def test_register_node_get_400_response_when_node_address_not_in_request(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.register_node_route, json=self.empty_json)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    # /add_model route =============================================================================================== #

    def test_add_model_get_200_when_user_is_registered(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        response = self.tester.post(self.add_model_route, json=self.json_for_adding_model)
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)

    def test_add_model_get_400_when_user_is_not_registered(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.add_model_route, json=self.json_for_adding_model)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    def test_add_model_get_400_when_user_does_not_give_node_address(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.add_model_route, json=self.empty_json)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    def test_add_model_does_add_model_to_block_chain_variable(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        self.tester.post(self.add_model_route, json=self.json_for_adding_model)
        # THEN
        self.assertIn(self.a_model, application.block_chain.models)

    # /mine route ==================================================================================================== #

    def test_mine_get_200_when_user_is_registered(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        response = self.tester.post(self.mine_route, json=self.register_json)
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)

    def test_mine_get_400_when_user_is_not_registered(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.mine_route, json=self.register_json)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    def test_mine_get_400_when_user_does_not_give_node_address(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.mine_route, json=self.empty_json)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    def test_mine_does_add_model_to_block_chain_from_models_variable(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        self.tester.post(self.add_model_route, json=self.json_for_adding_model)
        self.tester.post(self.mine_route, json=self.register_json)
        data_from_last_block = application.block_chain.get_last_block.data
        # THEN
        self.assertIn(self.a_model, data_from_last_block)

    def test_mine_does_add_all_models_to_block_chain(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        for model_to_add in self.models:
            self.tester.post(self.add_model_route,
                             json={
                                 "node_address": self.an_address,
                                 "model_to_add": model_to_add,
                             })
        self.tester.post(self.mine_route, json=self.register_json)
        data_from_all_blocks = []
        for a_block in application.block_chain.chain:
            data_from_all_blocks.append(a_block.data)
        # THEN
        for a_model in self.models:
            self.assertIn(a_model, data_from_all_blocks)

    # /get_chain route =============================================================================================== #

    def test_get_chain_get_200_when_user_is_registered(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        response = self.tester.post(self.get_chain_route, json=self.register_json)
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)

    def test_get_chain_get_400_when_user_is_not_registered(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.get_chain_route, json=self.register_json)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    def test_get_chain_get_400_when_user_does_not_give_node_address(self):
        # GIVEN
        # WHEN
        response = self.tester.post(self.get_chain_route, json=self.empty_json)
        # THEN
        self.assertEqual(self.return_code_400, response.status_code)

    def test_get_chain_does_return_current_version_of_block_chain(self):
        # GIVEN
        # WHEN
        self.tester.post(self.register_node_route, json=self.register_json)
        self.tester.post(self.add_model_route, json=self.json_for_adding_model)
        self.tester.post(self.mine_route, json=self.register_json)
        received_bock_chain = self.tester.post(self.get_chain_route, json=self.register_json)
        received_data = received_bock_chain.data.decode('utf-8')
        # THEN
        self.assertIn(self.a_model, str(received_data))

    # create_chain function ========================================================================================== #

    def test_create_chain_from_json_dumps_returns_correct_chain_for_genesis_block(self):
        # GIVEN
        expected_block_chain = [self.block_chain.chain[0]]
        # WHEN
        received_chain = application.create_chain_from_json_dumps([self.block_chain_as_json[0]])
        # THEN
        self.assertEqual(str(received_chain.chain), str(expected_block_chain))

    def test_create_chain_from_json_dumps_returns_correct_chain_for_a_chain_with_2_blocks(self):
        # GIVEN
        expected_block_chain = self.block_chain.chain
        # WHEN
        received_chain = application.create_chain_from_json_dumps(self.block_chain_as_json)
        # THEN
        self.assertEqual(str(received_chain.chain), str(expected_block_chain))


# ==================================================================================================================== #

if __name__ == '__main__':
    unittest.main()
