# A Block Chain for distributed Machine Learning

This project aims to create an API for training machine learning algorithms without sharing data.

## Getting Started

 * Clone the project :
```
git clone git@gitlab.com:padalous/block_chain_for_ml.git
```
* Install the requirements (virtualenv recommended)
```
cd block_chain_for_ml
pip install -r requirements
```
* Launch the server
```
python application.py
```
